import axios, {AxiosInstance, AxiosRequestConfig, AxiosResponse} from "axios";

class AxiosService {
    private instance: AxiosInstance;

    constructor() {
        this.instance = axios.create({
            baseURL: `${process.env.API_PROTOCOL}${process.env.API_DOMAIN}${process.env.API_PORT}`,
            timeout: 5000,
            headers: {
                'Content-type': 'application/json'
            }
        })
    }

    public setAuthorizationHeader(token: string): void {
        this.instance.defaults.headers['Authorization'] = token
    }

    public post<T>(url: string, data?: any, config?: AxiosRequestConfig): Promise<AxiosResponse<T>> {
        return this.instance.post<T>(url, {"data": data}, config);
    }
}

const axiosService = new AxiosService();
export default axiosService;