import React from 'react';
import ReactDOM from 'react-dom/client';
import Router from "./routes/route";
import {BrowserRouter} from "react-router-dom";
// import "./styles/main.sass"
import {Provider} from "react-redux";
import {persistor, store} from './app/store'
import {PersistGate} from "redux-persist/integration/react";
import "./index.css"

const root = ReactDOM.createRoot(
    document.getElementById('root') as HTMLElement
);
root.render(
    <React.StrictMode>
        <Provider store={store}>
            <PersistGate persistor={persistor} loading={null}>
                <BrowserRouter>
                    <Router/>
                </BrowserRouter>
            </PersistGate>
        </Provider>
    </React.StrictMode>
);

