import React, {useState} from "react";
import axiosService from "../services/axiosService";
import {ResponseAPIObject} from "../interfaces/responseAPI";
import {ProductInterface} from "../interfaces/productInterface";

interface FormulaireProps {
}

const Formulaire: React.FC<FormulaireProps> = () => {
    const [blob, setBlob] = useState<string>()
    const [name, setName] = useState<string>("")
    const [description, setDescription] = useState<string>("")
    const [price, setPrice] = useState<number>(0)
    const [stock, setStock] = useState<number>(0)
    const [categorie, setCategorie] = useState<string>("")

    const [product, setProduct] = useState<ProductInterface>()

    const onSelectImage = (event: React.ChangeEvent<HTMLInputElement>): void => {
        if (event.target.files && event.target.files.length > 0) {
            const blob = event.target.files[0]

            const reader = new FileReader()

            reader.onload = (e) => {
                const base64String = e.target?.result as string

                setBlob(base64String)
            }
            reader.readAsDataURL(blob)
        }
    }

    const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
        e?.preventDefault()

        const data: ProductInterface = {
            name: name,
            description: description,
            price: price,
            stock: stock,
            categorie: categorie,
            image: blob
        }

        axiosService.post<ResponseAPIObject<ProductInterface>>("/product/create", data)
            .then((res) => {
                console.log(res.status)
            })
            .catch((error: any) => {
                console.log(error)
            })

    }

    // useEffect(() => {
    //     const loadData = async () => {
    //         await axiosService.post<ResponseAPI<ProductInterface>>("/product/getAll")
    //             .then((res) => {
    //                 setProduct(res.data.Data[0])
    //             })
    //             .catch((error: any) => {
    //                 console.log(error)
    //             })
    //     }
    //
    //     loadData()
    // }, [])

    return (
        <form>
            {/*Name*/}
            <label htmlFor={"name"}>Name</label>
            <input type="text" name={"name"} onChange={(e) => setName(e.target.value)}/>
            {/*Description*/}
            <label htmlFor={"description"}>Description</label>
            <input type="text" name={"description"} onChange={(e) => setDescription(e.target.value)}/>
            {/*Price*/}
            <label htmlFor={"price"}>Price</label>
            <input type="number" name={"price"} onChange={(e) => setPrice(parseInt(e.target.value))}/>
            {/*Stock*/}
            <label htmlFor={"stock"}>Stock</label>
            <input type="number" name={"stock"} onChange={(e) => setStock(parseInt(e.target.value))}/>
            {/*Ctégorie*/}
            <label htmlFor={"categorie"}>Catégorie</label>
            <input type="text" name={"categorie"} onChange={(e) => setCategorie(e.target.value)}/>
            {/*Image*/}
            <label htmlFor="image">Image</label>
            <input type="file" name={"image"} onChange={(e: React.ChangeEvent<HTMLInputElement>) => onSelectImage(e)}/>

            <button onClick={handleClick}>Créer Article</button>
        </form>
    )
}

export default Formulaire