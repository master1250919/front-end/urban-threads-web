import React from "react";

interface ButtonProps {
    className?: string | undefined
    onClick?: () => void
    label: string
    type?: "button" | "submit" | "reset" | undefined
}

const Button: React.FC<ButtonProps> = ({onClick, label, type, className}) => {
    return (
        <>
            <button className={className} type={type} onClick={onClick}>{label}</button>
        </>
    )
}

export default Button