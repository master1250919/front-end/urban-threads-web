import React from "react";

interface CardProps {
    image: string | undefined
    name: string
    description: string
    price: number
}

const Card: React.FC<CardProps> = ({image, name, description, price}) => {
    return (
        <main className={"card"}>
            <img src={image} alt="Image du produit"/>
            <div className="card-content">
                <p className={"card-name"}>{name}</p>
                <p>{description}</p>
                <p>{price}</p>
            </div>
        </main>
    )
}

export default Card