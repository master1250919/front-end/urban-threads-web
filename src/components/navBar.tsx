import React from "react";
import Menu from "./menu";
import {Link} from "react-router-dom";
import {useAppSelector} from "../app/hooks";
import {selectUser} from "../features/user/userSlice";
import Button from "./button";
import {selectLogin} from "../features/user/loginSlice";
import {UserInterface} from "../interfaces/userInterface";

interface NavBarProps {
    inLoginPage?: boolean
    setTrigger?: React.Dispatch<React.SetStateAction<boolean>>
}

const NavBar: React.FC<NavBarProps> = ({inLoginPage = false, setTrigger}) => {
    const user: UserInterface = useAppSelector(selectUser)
    const isLogin: boolean = useAppSelector(selectLogin)

    return (
        <div className={"flex flex-row justify-center items-center px-20 py-0"}>
            <div className={"flex flex-col justify-between items-center m-auto"}>
                {inLoginPage ? (
                    <>
                        <p className={"text-color font-Horizon"}>UT</p>
                    </>
                ) : (
                    <>
                        <p><Link to={"/home"} className={"text-color font-Horizon"}>UT</Link></p>
                    </>
                )}
                <p className={"text-color text-sm font-MontserratBold"}>Urban Threads</p>
            </div>
            {(user.is_admin && isLogin) && (
                <>
                    <Button className={"mx-3"} label={"New Products"} onClick={() => setTrigger!(true)}/>
                </>
            )}
            {inLoginPage ? (
                <></>
            ) : (
                <>
                    <Menu className={"text-color hover:cursor-pointer"}/>
                </>
            )}
        </div>
    )
}

export default NavBar