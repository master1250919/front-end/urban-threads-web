import React, {useState} from "react"
import {FiMenu} from "react-icons/fi"
import {RxCross2} from "react-icons/rx"
import {Link, useNavigate} from "react-router-dom"
import {useAppDispatch} from "../app/hooks";
import {setLogOut} from "../features/user/loginSlice";

interface MenuProps {
    className?: string;
}

const Menu: React.FC<MenuProps> = ({className}) => {
    // STATE
    const [isOpen, setIsOpen] = useState(false)
    const [isCollection, setIsCollection] = useState(false)

    // VAR
    const dispatch = useAppDispatch()
    const navigate = useNavigate()

    // FUNC
    const toggleMenu = (): void => {
        setIsOpen(!isOpen)
    }

    const toggleCollection = (): void => {
        setIsCollection(!isCollection)
    }

    const onLogOut = () => {
        dispatch(setLogOut())
        navigate('/')
    }


    return (
        <>
            <FiMenu className={className} onClick={toggleMenu}/>
            <div
                className={isOpen ? 'flex flex-row justify-between items-start absolute top-0 right-0 h-full w-1/5 bg-background-menu px-20 py-0 font-MontserratBold' : 'hidden'}>
                <ul className={"flex flex-col justify-center items-start relative list-none mt-24 w-full"}>
                    <li className={"mb-7 hover:cursor-pointer"} onClick={toggleCollection}>Collection</li>
                    <ul className={isCollection ? "flex flex-col justify-between items-start pl-6" : "hidden"}>
                        <li className={"mb-5"}><Link to={"/collection/sweat"}>Sweat</Link></li>
                        <li className={"mb-5"}><Link to={"/collection/pants"}>Pants</Link></li>
                        <li className={"mb-5"}><Link to={"/collection/jackets"}>Jackets</Link></li>
                        <li className={"mb-5"}><Link to={"/collection/tshirts"}>T-shirts</Link></li>
                    </ul>
                    <li><Link to={"/account"}>Account</Link></li>
                    <li><Link to={"/settings"}>Settings</Link></li>
                    <li onClick={onLogOut}>Log Out</li>
                </ul>
                <RxCross2 className={"h-5 w-5 mt-3 hover:cursor-pointer"} onClick={toggleMenu}/>
            </div>
        </>
    )
}

export default Menu;