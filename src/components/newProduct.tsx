import React from "react";

interface newProductProps {
    setTrigger: React.Dispatch<React.SetStateAction<boolean>>
}

const NewProduct: React.FC<newProductProps> = ({setTrigger}) => {
    return (
        <div>
            <h1 onClick={() => setTrigger!(false)}>Nouveau Produit</h1>
        </div>
    )
}

export default NewProduct