import React from "react";
import NavBar from "../components/navBar";

interface SettingsProps {

}

const Settings: React.FC<SettingsProps> = () => {
    return (
        <main className={"bg-background-first w-screen h-screen"}>
            <NavBar/>
            <h1>Settings</h1>
        </main>
    )
}

export default Settings;