import React from "react";
import NavBar from "../components/navBar";
import {useParams} from "react-router-dom";

interface CollectionProps {

}

const Collection: React.FC<CollectionProps> = () => {

    let {parametre} = useParams()

    return (
        <main className={"bg-background-first w-screen h-screen"}>
            <NavBar/>
            {parametre}
        </main>
    )
}

export default Collection;