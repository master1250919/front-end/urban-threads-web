import NavBar from "../components/navBar";
import {SubmitHandler, useForm} from "react-hook-form";
import {LoginInterface} from "../interfaces/loginInterface";
import {Link, NavigateFunction, useNavigate} from "react-router-dom";
import Button from "../components/button";
import React, {useEffect, useState} from "react";
import {useAppDispatch, useAppSelector} from "../app/hooks";
import axiosService from "../services/axiosService";
import {ResponseAPIObject, ResponseErrorAPI} from "../interfaces/responseAPI";
import {AxiosError} from "axios";
import {changeLogin, selectLogin} from "../features/user/loginSlice";
import {UserInterface} from "../interfaces/userInterface";
import {setUserData} from "../features/user/userSlice";

interface LoginProps {

}

const Login: React.FC<LoginProps> = () => {

    // STATES
    const [isError, setIsError] = useState<boolean>(false)
    const [errorMessage, setErrorMessage] = useState<string | undefined>()

    // VAR
    const navigate: NavigateFunction = useNavigate();
    const dispatch = useAppDispatch()
    const isLogin: boolean = useAppSelector(selectLogin)
    const {register, handleSubmit} = useForm<LoginInterface>()

    // FUNC
    const onSubmit: SubmitHandler<LoginInterface> = (data, event) => {
        event?.preventDefault()

        axiosService.post<ResponseAPIObject<UserInterface>>("/user/login", data)
            .then((res) => {
                if (res.data.Success) {
                    dispatch(setUserData(res.data.Data))
                    dispatch(changeLogin(true))
                    navigate("/home")
                }
            })
            .catch((error: AxiosError<ResponseErrorAPI>) => {
                setIsError(true)
                setErrorMessage(error.response?.data.Data)
            })
    }

    useEffect(() => {
        if (isLogin) {
            navigate("/home")
        }
    }, [])

    return (
        <main className={"bg-background-first w-screen h-screen"}>
            <NavBar inLoginPage={true}/>

            <div className={"flex flex-col justify-center items-center h-4/5"}>
                <h2 className={"font-Horizon mb-7"}>Connexion</h2>
                <form
                    className={"flex flex-col justify-center items-center bg-background-menu p-10 w-1/3 shadow-xl rounded"}
                    onSubmit={handleSubmit(onSubmit)}>
                    <div className="p-2.5 w-full">
                        <input className={"w-full rounded p-1"} type="text" {...register("email")}
                               placeholder={"Email"}/>
                    </div>
                    <div className="p-2.5 w-full">
                        <input className={"w-full rounded p-1"} type="password" {...register("password")}
                               placeholder={"Password"}/>
                    </div>

                    {isError && (
                        <>
                            <p>{errorMessage}</p>
                        </>
                    )}

                    <Button className={"text-normal font-MontserratBold"} label={"Connexion"} type={"submit"}/>
                    <Link to={"createAccount"} className={"text-sm font-MontserratMedium"}>Créer un compte</Link>
                </form>
            </div>
        </main>
    )
}

export default Login