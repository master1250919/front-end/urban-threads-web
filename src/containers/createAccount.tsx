import React, {useState} from "react";
import NavBar from "../components/navBar";
import {SubmitHandler, useForm} from "react-hook-form";
import {CreateAccountInterface} from "../interfaces/createAccountInterface";
import Button from "../components/button";
import axiosService from "../services/axiosService";
import {ResponseAPIObject, ResponseErrorAPI} from "../interfaces/responseAPI";
import {UserInterface} from "../interfaces/userInterface";
import {AxiosError} from "axios";
import {useAppDispatch} from "../app/hooks";
import {setUserData} from "../features/user/userSlice";
import {NavigateFunction, useNavigate} from "react-router-dom";

interface CreateAccountProps {

}

const CreateAccount: React.FC<CreateAccountProps> = () => {
    const [isError, setIsError] = useState<boolean>(false)
    const [errorMessage, setErrorMessage] = useState<string | undefined>()

    const {register, handleSubmit} = useForm<CreateAccountInterface>()
    const dispatch = useAppDispatch()
    const navigate: NavigateFunction = useNavigate()

    const OnSubmit: SubmitHandler<CreateAccountInterface> = (data, event) => {
        event?.preventDefault()

        axiosService.post<ResponseAPIObject<UserInterface>>("/user/create", data)
            .then((res) => {
                if (res.data.Success) {
                    dispatch(setUserData(res.data.Data))
                    navigate("/")
                }
            })
            .catch((error: AxiosError<ResponseErrorAPI>) => {
                setIsError(true)
                setErrorMessage(error.response?.data.Data)
            })
    }

    return (
        <main className={"bg-background-first w-screen h-screen"}>
            <NavBar inLoginPage={true}/>

            <h2>Création de compte</h2>

            <form className={"form-container"} onSubmit={handleSubmit(OnSubmit)}>
                <div className="form-data">
                    <label>Nom</label>
                    <input type="text" {...register("name")}/>
                </div>
                <div className="form-data">
                    <label>Email</label>
                    <input type="text" {...register("email")}/>
                </div>
                <div className="form-data">
                    <label>Password</label>
                    <input type="password" {...register("password")}/>
                </div>
                <div className="form-data">
                    <label>Phone</label>
                    <input type="text" {...register("phone")}/>
                </div>

                {isError && (
                    <>
                        <p>{errorMessage}</p>
                    </>
                )}

                <Button label={"Créer"} type={"submit"}/>
            </form>
        </main>
    )
}

export default CreateAccount