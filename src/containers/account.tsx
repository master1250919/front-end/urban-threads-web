import React from "react";
import NavBar from "../components/navBar";

interface AccountProps {

}

export const Account: React.FC<AccountProps> = () => {
    return (
        <main className={"bg-background-first w-screen h-screen"}>
            <NavBar/>
            <h1>Account</h1>
        </main>
    )
}