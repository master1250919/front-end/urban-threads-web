import React, {useEffect, useState} from "react";
import NavBar from "../components/navBar";
import Card from "../components/card";
import axiosService from "../services/axiosService";
import {useAppSelector} from "../app/hooks";
import {ResponseAPIArray} from "../interfaces/responseAPI";
import {ProductInterface} from "../interfaces/productInterface";
import {AxiosError} from "axios";
import {selectUser} from "../features/user/userSlice";
import NewProduct from "../components/newProduct";

interface HomeProps {

}

const Home: React.FC<HomeProps> = () => {
    const [products, setProducts] = useState<Array<ProductInterface>>()
    const [trigger, setTrigger] = useState<boolean>(false)

    const user = useAppSelector(selectUser)

    useEffect(() => {
        axiosService.setAuthorizationHeader(user.token)
        axiosService.post<ResponseAPIArray<ProductInterface>>("/product/getAll")
            .then((res) => {
                setProducts(res.data.Data)
            })
            .catch((error: AxiosError<ProductInterface>) => {
                console.log(error)
            })
    }, [])

    return (
        <main className={"bg-background-first w-screen h-screen"}>
            <NavBar setTrigger={setTrigger}/>
            <h1>Home</h1>
            {products?.map((value) => {
                return (
                    <div className={"card-container"}>
                        <Card image={value.image} name={value.name} description={value.description}
                              price={value.price}/>
                    </div>
                )
            })}
            {trigger && (
                <NewProduct setTrigger={setTrigger}/>
            )}
        </main>
    )
}

export default Home