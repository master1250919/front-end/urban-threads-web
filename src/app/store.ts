import {AnyAction, combineReducers, configureStore} from "@reduxjs/toolkit";
import {persistReducer, persistStore} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import {FLUSH, PAUSE, PERSIST, PURGE, REGISTER, REHYDRATE} from "redux-persist/es/constants";
import {userSlice} from "../features/user/userSlice";
import {loginSlice} from "../features/user/loginSlice";

const persistConfig = {
    key: "root",
    version: 1,
    storage,
}

const reducers = combineReducers({
    user: userSlice.reducer,
    isLogin: loginSlice.reducer
})

const rootReducer = (state: any, action: AnyAction) => {
    if (action.type === "logOut/setLogOut") {
        storage.removeItem("persist:root")

        return reducers(undefined, action)
    } else {
        return reducers(state, action)
    }
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    devTools: true,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
            serializableCheck: {
                ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
            }
        }),
})

export let persistor = persistStore(store)
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch