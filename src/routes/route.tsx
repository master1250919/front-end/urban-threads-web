import React from "react";
import {Route, Routes} from "react-router-dom"
import Home from "../containers/home"
import Collection from "../containers/collection";
import Settings from "../containers/settings";
import Login from "../containers/login";
import {Account} from "../containers/account";
import CreateAccount from "../containers/createAccount";

const Router: React.FC = () => {
    return (
        <Routes>
            <Route path={"/"} element={<Login/>}/>
            <Route path={"/createAccount"} element={<CreateAccount/>}/>
            <Route path={"/home"} element={<Home/>}/>
            <Route path={"/collection/:parametre"} element={<Collection/>}/>
            <Route path={"/account"} element={<Account/>}/>
            <Route path={"/settings"} element={<Settings/>}/>
        </Routes>
    );
}

export default Router