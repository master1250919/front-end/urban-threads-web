export interface ResponseAPIArray<T> {
    Data: Array<T>
    Status: number,
    Success: boolean
}

export interface ResponseAPIObject<T> {
    Data: T
    Status: number
    Success: boolean
}

export interface ResponseErrorAPI {
    Data: string
    Status: number
    Success: boolean
}