export interface UserInterface {
    id: string,
    name: string,
    email: string,
    password: string,
    phone: string,
    is_admin: boolean
    token: string
}