export interface CreateAccountInterface {
    name: string
    email: string
    password: string
    phone: string
}