export interface ProductInterface {
    id?: string,
    name: string,
    description: string,
    price: number,
    stock: number,
    categorie: string,
    image: string | undefined
}