import {createSlice} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";
import {UserInterface} from "../../interfaces/userInterface";

export const userSlice = createSlice({
    name: "user",
    initialState: {} as UserInterface,
    reducers: {
        setUserData: (state, action) => {
            return {...state, ...action.payload}
        }
    }
})

export const {setUserData} = userSlice.actions
export const selectUser = (state: RootState) => state.user