import {createSlice} from "@reduxjs/toolkit";
import {RootState} from "../../app/store";

export const loginSlice = createSlice({
    name: "isLogin",
    initialState: false,
    reducers: {
        changeLogin: (state, action) => {
            return action.payload
        }
    }
})

export const logOutSlice = createSlice({
    name: "logOut",
    initialState: [],
    reducers: {
        setLogOut: () => {
        }
    }
})

export const {changeLogin} = loginSlice.actions
export const selectLogin = (state: RootState) => state.isLogin

export const {setLogOut} = logOutSlice.actions