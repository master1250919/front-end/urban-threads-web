const path = require('path')
const htmlWebpackPlugin = require('html-webpack-plugin')
const Dotenv = require('dotenv-webpack')

module.exports = () => {

    return ({
        entry: "./src/index.tsx",
        target: "web",
        mode: "development",
        output: {
            path: path.resolve(__dirname, "build")
        },
        devServer: {
            historyApiFallback: true
        },
        plugins: [
            new htmlWebpackPlugin({
                template: "./public/index.html"
            }),
            new Dotenv({
                path: `.env`,
                systemvars: true
            })
        ],
        resolve: {
            extensions: [".js", ".ts", ".tsx", ".jsx"]
        },
        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/,
                    exclude: /node_modules/,
                    use: 'ts-loader'
                },
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: ['@babel/preset-env', '@babel/preset-react', '@babel/preset-typescript']
                        }
                    }
                },
                {
                    test: /\.css$/i,
                    use: [
                        "style-loader",
                        "css-loader",
                        "postcss-loader"
                    ]
                },
                {
                    test: /\.(otf|ttf)$/,
                    type: "asset/resource"
                }
            ]
        }
    })
}