/** @type {import('tailwindcss').Config} */
module.exports = {
    content: [
        "./src/**/*.{js,jsx,ts,tsx}"
    ],
    theme: {
        extend: {},
        colors: {
            'background-first': '#BFB29E',
            'background-menu': '#B3A492',
            'color': '#272829',
        },
        fontSize: {
            'sm': '10px',
            'normal': '12px'
        },
        fontFamily: {
            "Horizon": ["Horizon"],
            "MontserratBold": ["MontserratBold"],
            "MontserratMedium": ["MontserratMedium"]
        }
    },
    plugins: [],
}

